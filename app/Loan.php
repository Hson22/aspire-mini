<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $table = 'loan';

    protected $fillable = [
        'id',
        'user_id',
        'loan',
        'interest',
        'duration',
        'rate',
        'total_loan',
        'loan_status',
    ];

    public function payment()
    {
        return $this->hasMany('App\Payment');
    }
}
