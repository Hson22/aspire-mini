<?php

namespace App\Http\Controllers;

use App\AdminModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{


    function __construct(Request $request)
    {
        $this->request = $request->json()->all();
        $this->response = [];
    }

    public function register()
    {
        $user = new User();
        $this->request['password'] = Hash::make($this->request['password']);
        $user->fill($this->request);
        $user->save();
        return $user;
    }


}
