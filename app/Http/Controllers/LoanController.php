<?php

namespace App\Http\Controllers;

use App\Loan;
use App\Payment;
use App\RateModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class LoanController extends Controller
{


    function __construct(Request $request)
    {
        $this->request = $request->json()->all();
        $this->response = [];
    }


    private function createLoanObject($user)
    {
        $newLoan = new Loan();
        $newLoan->user_id = $user->id;
        $newLoan->loan = $this->request['loan'];
        $newLoan->rate = $this->findRate($this->request['loan_duration'], $this->request['loan']);
        $newLoan->duration = $this->request['loan_duration'];
        $newLoan->interest = $newLoan->rate * $this->request['loan_duration'];
        $newLoan->total_loan = $newLoan->loan * (1 + $newLoan->interest);
        $newLoan->payment_date = Carbon::today()->addMonth(1);
        $newLoan->monthly_bill = $newLoan->total_loan / $newLoan->duration;
        $newLoan->loan_status = 'request';
        $newLoan->save();
        return $newLoan;
    }

    public function createLoan()
    {
        $user = User::where('email', $this->request['email'])->first();

        if (empty($user)) {
            $this->response['status'] = 'failed';
            $this->response['message'] = 'user not found';
        } else {
            $loanRecord = $this->checkUserLoan($user->id);
            if (!$loanRecord) { // assumption that user can only have 3 active loan
                $this->response['status'] = 'failed';
                $this->response['message'] = 'you have 3 loan that not finished yet';
            } else {
                $this->createLoanObject($user);
                $this->response['status'] = 'success';
                $this->response['message'] = 'thankyou';
            }
        }
        return $this->response;

    }
    private function findRate($duration, $loan)
    {
        $interest = RateModel::where('minimum_loan', '<=', $loan)->where('maximum_loan', '>=', $loan)->where('duration', $duration)->first();
        return $interest->rate;

    }

    public function getUserLoan($email)
    {

        $user = User::where('email', $email)->first();
        if ($user) {
            $loan = Loan::where('user_id', $user->id)->get();
            $this->response['status'] = 'success';
            $this->response['message'] = 'get list loan';
            $this->response['loan_history'] = $loan;
        } else {
            $this->response['status'] = 'failed';
            $this->response['message'] = 'user not found';
        }
        return $this->response;
    }


    public function paymentHistory($email,$loanId)
    {
        $paymentHistory = Loan::with('payment')->where('id', $loanId)->where('email',$email)->first();
        $this->response['status'] = 'success';
        $this->response['message'] = 'success get list';
        $this->response['history'] = $paymentHistory;
        return $this->response;
    }

    private function checkUserLoan($user_id)
    {
        $loanHistory = Loan::where('user_id', $user_id)->where('loan_status', '!=', 'complete')->count();
        if ($loanHistory >= 3) {
            $status = false;
        } else {
            $status = true;
        }
        return $status;
    }

    public function payment()
    {
        $loan = Loan::where('id', $this->request['id'])->where('user_id', $this->request['user_id'])->first();
        $newPayment = new Payment();
        $newPayment->loan_id = $loan->id;
        if ($loan->loan_status == 'complete') {
            $this->response['status'] = 'failed';
            $this->response['message'] = 'your loan has been fully paid';
            return $this->response;
        } elseif($loan->loan_status == 'request') {
            $this->response['status'] = 'failed';
            $this->response['message'] = 'your loan hasnt been approved';
            return $this->response;
        }
        if ($loan->total_loan >= $this->request['payment']) {
            $loan->total_loan = $loan->total_loan - $this->request['payment'];
            $newPayment->payment = $this->request['payment'];
            if ($loan->total_loan == 0) {
                $loan->loan_status = 'complete';
            }
            $loan->save();
            $newPayment->save();
        } else {
            $this->response['status'] = 'success';
            $this->response['message'] = 'monthly payment didnt match';
            return $this->response;
        }
        $loan->payment_date = Carbon::parse($loan->payment_date)->addMonth(1)->toDateString();
        $loan->save();
        $this->response['status'] = 'success';
        $this->response['message'] = 'your payment have been received, thankyou';
        return $this->response;
    }

    public function approveLoan()
    {
        $loan = Loan::where('id',$this->request['id'])->where('status','request')->first();
        if($loan){
            $loan->loan_status = 'approved';
            $loan->save();
            $this->response['status'] = 'success';
            $this->response['message'] = 'success approved loan';
        } else {
            $this->response['status'] = 'failed';
            $this->response['message'] = 'failed approved loan';
        }

        return $this->response;
    }

    public function allRequest()
    {
        $loan = Loan::where('status','request')->get();
        $this->response['status'] = 'success';
        $this->response['message'] = 'success aprove loan';
        $this->response['data'] = $loan;
        return $this->response;

    }

    public function rejectLoan()
    {
        $loan = Loan::where('id',$this->request['id'])->where('status','request')->first();
        if($loan){
            $loan->loan_status = 'reject';
            $loan->save();
            $this->response['status'] = 'success';
            $this->response['message'] = 'success reject loan';
        } else {
            $this->response['status'] = 'failed';
            $this->response['message'] = 'failed reject loan';
        }

        return $this->response;
    }
}
