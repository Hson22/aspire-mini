<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\AdminModel;
use Hash;

class LoginController extends Controller
{
    function __construct(Request $request)
    {
        $this->request = $request->json()->all();
        $this->response = [];
    }
    public function login()
    {
        $user = User::where('email',$this->request['email'])->first();
        if(empty($user)) {
            $this->response['status']='failed';
            $this->response['message'] = 'user not found';
            return $this->response;
        }
        if(!Hash::check($this->request['password'],$user->password)) {
            $this->response['status']='failed';
            $this->response['message'] = 'password doesnt match';
            return $this->response;
        }
        $token = Hash::make('saltString'.$user->email);
        $user->remember_token = $token;
        $user->save();
        $this->response['status'] = 'success';
        $this->response['message'] = 'login success';
        $this->response['user'] = $user;
        return $this->response;
    }
    public function adminLogin()
    {
        $user = AdminModel::where('email',$this->request['email'])->first();
        if(empty($user)) {
            $this->response['status']='failed';
            $this->response['message'] = 'user not found';
            return $this->response;
        }
        if(!Hash::check($this->request['password'],$user->password)) {
            $this->response['status']='failed';
            $this->response['message'] = 'password doesnt match';
            return $this->response;
        }
        $token = Hash::make('saltString'.$user->email);
        $user->remember_token = $token;
        $user->save();
        $this->response['status'] = 'success';
        $this->response['message'] = 'login success';
        $this->response['user'] = $user;
        return $this->response;
    }
}
