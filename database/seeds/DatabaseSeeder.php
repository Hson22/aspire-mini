<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin')->insert([
            [
                'name' => 'Super Admin',
                'email' => 'superadmin',
                'password' => Hash::make('sharedream123')
            ]
        ]);
    }
}
