<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Loan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan',function(Blueprint $table){
            $table->integer('id')->autoIncrement();
            $table->integer('user_id');
            $table->float('loan',9,2);
            $table->tinyInteger('duration');
            $table->float('rate');
            $table->float('total_loan',9,2);
            $table->float('monthly_bill',9,2);
            $table->float('interest',9,2);
            $table->date('payment_date');
            $table->text('loan_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan');
    }
}
