<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/register','UserController@register');//done
Route::get('/loan/{email}','LoanController@getUserLoan');//done
Route::post('payment','LoanController@payment');//done
Route::get('payment/{email}/{loanId}','LoanController@paymentHistory');
Route::post('/loan','LoanController@createLoan');//done
Route::post('/login','UserController@login');//done


//admin
Route::post('/admin/login','LoginController@adminLogin');//done
Route::post('/loan/approve','LoanController@approveLoan');
Route::post('/loan/reject','LoanController@reject');
